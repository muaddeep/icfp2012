#!/usr/bin/env python
import sys, os, time
import random
import copy
import signal

#XXX: I don't know this fucking snake .. so .. sorry :-D

class MineCraft:
    def __init__(self,minemap):
        self.debug = True
        # XXX: Y coordinate comes first ... || we need to transform matrix. on the other hand ... operations should be correct
        # XXX: loops can be merged ... but not in 2:00
        # XXX: I'm really sorry for this peace of crap

        # minemap -> current map
        self.minemap_ = minemap

        # Flooding extension support:
        self.water = 0
        self.flooding = 0 
        self.waterproof = 10 

        
        # minemap size
        self.y_max = self.minemap_.__len__() - 1
        self.x_max = self.minemap_[0].__len__() -1
        for line in self.minemap_:
            #print line + str(line.__len__())
            if line.__len__() - 1  > self.x_max and line.split(' ')[0] not in ['Water', 'Flooding', 'Waterproof', 'Trampoline', 'Growth', 'Razors']:
                self.x_max = line.__len__() - 1


        # if map have strange form. fill with space end of lines up to max length
        for j in range(self.minemap_.__len__()):
            if self.minemap_[j].__len__() < (self.x_max + 1):
                self.minemap_[j] = self.minemap_[j] + ' ' * (self.x_max - self.minemap_[j].__len__() + 1)
            #if self.debug:
            #    print self.minemap_[j] + '|' + str(self.minemap_[j].__len__())

        # growh extenxion support:
        self.growth = 25
        self.razors = 0
        #transform to matrix
        self.targets = {}
        self.minemap = []
        for j in range(self.y_max, -1, -1):
            a = self.minemap_[j].split(' ')
            if a[0] == 'Water':
                self.water = int(a[1])
            elif a[0] == 'Flooding':
                self.flooding = int(a[1])
            elif a[0] == 'Waterproof':
                self.waterproof = int(a[1])
            elif a[0] == 'Trampoline':
                self.targets[a[1]] = a[3]
            elif a[0] == 'Growth':
                self.growth = int(a[1])
            elif a[0] == 'Razors':
                self.razors = int(a[1])
            elif not self.minemap_[j].strip():
            # ommit blank lines
                pass
            else:
                self.minemap.append(list(self.minemap_[j]))
        self.current_growth = self.growth - 1

        # minemap size
        self.y_max = self.minemap.__len__() - 1
        self.x_max = self.minemap[0].__len__() -1
        for line in self.minemap:
            #print line + str(line.__len__())
            if line.__len__() - 1  > self.x_max:
                self.x_max = line.__len__() - 1

        # for bot killing we need to know the time when block changed
        # initialized with 0, but when map is changed should be changed 
        self.map_time_tracker = [[ 0 for j in range(self.y_max + 1)] for i in range(self.x_max + 1)]

        # Flooding ext counters
        self.current_water = self.water
        self.current_underwater_time = 0
        self.current_flooding = self.flooding 

        # initial bot coordinates and count lambdas
        self.beards = []
        self.custom_object_coordinates = {}
        self.lambdas_total = 0
        for i in range(self.x_max):
            for j in range(self.y_max):
                if self.minemap[j][i] == "R":
                    self.x_bot = i
                    self.y_bot = j
                if self.minemap[j][i] == '\\' or self.minemap[j][i] == '@':
                    self.lambdas_total += 1
                if str(self.minemap[j][i]) in ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', '1', '2', '3', '4', '5', '6', '7', '8', '9']:
                    self.custom_object_coordinates[self.minemap[j][i]] = [i, j]
                if self.minemap[j][i] == 'W':
                    self.beards.append([i,j])


        self.size = (self.x_max + 1) * (self.y_max + 1)
        self.score = 0
        self.lambdas_gathered = 0
        self.turn_number = 0
        self.abort = False
        self.win = False
        self.loose = False
        self.status_applied = False

    def turn(self, command):
        if self.loose or self.win or self.abort:
            return
        self.turn_number += 1
        self.score -= 1
        if command == 'L':
            self.turn_attempt(self.x_bot - 1, self.y_bot)
        elif command == 'R':
            self.turn_attempt(self.x_bot + 1, self.y_bot)
        elif command == 'U':
           if self.debug:
                print 'bot location ' + str(self.x_bot) + ' ' + str(self.y_bot)
           self.turn_attempt(self.x_bot, self.y_bot + 1)
        elif command == 'D':
            self.turn_attempt(self.x_bot, self.y_bot - 1)
        elif command == 'A':
            self.abort = True
        elif command == 'W':
            pass
        elif command == 'S':
            if self.razors > 0:
                self.razors -= 1
                for k in range (-1,2):
                    for l in range(-1,2):
                        if self.minemap[self.y_bot + l][self.x_bot + k] == 'W':
                            self.minemap[self.y_bot + l][self.x_bot + k] = ' '

        else:
            if self.debug:
                print 'Unknown command: ' + command
            pass


    def turn_attempt(self, x,y):
        if self.debug:
            print 'turn attempt: ' + str(self.x_bot) + '=>' + str(x) + ' ' + str(self.y_bot) + '=>' + str(y)
        #'Empty || Earth'
        if self.minemap[y][x] == ' ' or self.minemap[y][x] == '.':
            self.empty_location(self.x_bot, self.y_bot)
            self.set_bot_location(x, y)
        #Lambda
        elif self.minemap[y][x] == '\\':
            self.empty_location(self.x_bot, self.y_bot)
            self.set_bot_location(x, y)
            self.lambdas_gathered += 1
            self.score += 25
       #Open lambda lift => this will replace lift with bot, however, we'll set win flag ...
        elif self.minemap[y][x] == 'O':
            self.empty_location(self.x_bot, self.y_bot)
            self.set_bot_location(x, y)
            self.win = True
        #crap 1
        elif (x == self.x_bot + 1) and (y == self.y_bot) and self.minemap[y][x] == '*' and self.minemap[y][self.x_bot + 2 ] == ' ':
            self.empty_location(self.x_bot, self.y_bot)
            self.minemap[y][self.x_bot + 2 ] = '*'
            self.set_bot_location(x, y)
        #crap2
        elif (x == self.x_bot - 1) and (y == self.y_bot) and self.minemap[y][x] == '*' and self.minemap[y][self.x_bot - 2 ] == ' ':
            self.empty_location(self.x_bot, self.y_bot)
            self.minemap[y][self.x_bot - 2] = '*'
            self.set_bot_location(x, y)
        elif self.minemap[y][x] in ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I']:
            self.empty_location(self.x_bot, self.y_bot)
            target = self.targets[self.minemap[y][x]]
            new_location_x = self.custom_object_coordinates[target][0]
            new_location_y = self.custom_object_coordinates[target][1]
            self.set_bot_location(new_location_x, new_location_y)
            for item, value in self.targets.iteritems():
                if value == target:
                    self.empty_location(self.custom_object_coordinates[item][0], self.custom_object_coordinates[item][1])
        elif self.minemap[y][x] == '!':
            self.empty_location(self.x_bot, self.y_bot)
            self.set_bot_location(x, y)
            self.razors += 1
        else:
            if self.debug:
                print 'not valid turn'
        #not valid turn .. do nothing?
            pass

    def empty_location(self, x, y):
        self.minemap[y][x] = ' '

    def set_bot_location(self, x, y):
        self.x_bot = x
        self.y_bot = y
        self.minemap[y][x] = 'R'

    def rebuild(self):
        if self.loose or self.win or self.abort:
            return

        self.map_time_tracker = []
        self.map_time_tracker = [[ 0 for j in range(self.y_max + 1)] for i in range(self.x_max + 1)]

        if self.flooding > 0:
            self.current_flooding -= 1
            if self.current_flooding == 0:
                self.current_water += 1
                self.current_flooding = self.flooding
            #TODO: not sure if it should happen here ..  if teritory flooded, should this start counting on the next turn?
            if self.y_bot <= self.current_water:
                self.current_underwater_time += 1
            else:
                self.current_underwater_time = 0



        # for  gathering map update transaction
        update_queue = []
        for i in range(self.x_max + 1):
            for j in range(self.y_max + 1):
                if self.is_rock(i,j) and self.minemap[j-1][i] == ' ':
                    update_queue.append([i, j, ' '])
                    update_queue.append([i, j - 1, self.minemap[j][i]])
                    self.map_time_tracker[i][j-1] = 1
                    if self.allowed_rock_lambda_transformation(i, j, i , j-1):
                        update_queue.append([i, j-1, '\\'])

                elif self.is_rock(i,j) and self.is_rock(i, j-1) and self.minemap[j][i+1] == ' ' and self.minemap[j - 1][i + 1] == ' ':
                    update_queue.append([i, j, ' '])
                    update_queue.append([i+1, j-1, self.minemap[j][i]])
                    self.map_time_tracker[i+1][j-1] = 1;
                    if self.allowed_rock_lambda_transformation(i, j, i+1, j-1):
                        update_queue.append([i+1, j-1, '\\'])

#TODO: not sure about either ...
                elif self.is_rock(i,j) and self.is_rock(i, j-1) and  (self.minemap[j][i+1] != ' ' or self.minemap[j-1][i+1] != ' ') and self.minemap[j][i-1] == ' ' and self.minemap[j-1][i-1] == ' ':
                    update_queue.append([i, j, ' '])
                    update_queue.append([i-1, j-1, self.minemap[j][i]])
                    self.map_time_tracker[i-1][j-1] = 1
                    if self.allowed_rock_lambda_transformation(i, j, i-1, j-1):
                        update_queue.append([i-1, j-1, '\\'])

                elif self.is_rock(i,j) and self.minemap[j-1][i] == '\\' and self.minemap[j][i+1] == ' ' and self.minemap[j - 1] [ i + 1] == ' ':
                    update_queue.append([i, j, ' '])
                    update_queue.append([i+1, j-1, self.minemap[j][i]])
                    self.map_time_tracker[i+1][j-1] = 1
                    if self.allowed_rock_lambda_transformation(i, j, i+1, j-1):
                        update_queue.append([i+1, j-1, '\\'])

                elif self.minemap[j][i] == 'L' and self.lambdas_total - self.lambdas_gathered == 0:
                    update_queue.append([i, j, 'O'])
                    
        for (i, j, status) in update_queue:
            self.minemap[j][i] = status
       
        #TODO: technically that's a bug, but we assume that stones fall before growing .. not 100% correct 
        self.current_growth -= 1
        if self.current_growth < 0:
            self.current_growth = self.growth - 1
            if self.beards.__len__() > 0:
                new_beards = []
                for beard in self.beards:
                    for k in range(-1, 2):
                        for l in range(-1, 2):
                           # print k
                           # print l
                           # print '(' + str(beard[0]+k) + ',' + str(beard[1]+l) + ')'
                            if self.minemap[beard[1] + l][beard[0] + k] == ' ':
                                self.minemap[beard[1] + l][beard[0] + k] = 'W'
                                new_beards.append([beard[0] + k, beard[1] + l])
                self.beards += new_beards
               # print 'beards: '
               # print self.beards

    def allowed_rock_lambda_transformation(self,x,y,x_new, y_new):
        return self.minemap[y][x] == '@' and self.minemap[y_new-1][x_new] != ' '

    def is_rock(self,x,y):
        return self.minemap[y][x] == '*' or self.minemap[y][x] == '@'

    def end_of_turn(self):
        #print self.x_bot
        #print self.y_bot
        #print self.x_max
        #print self.y_max
        #print self.map_time_tracker[self.x_bot].__len__()

        if self.status_applied:
            return
        if self.win == True:
            self.score += 50 * self.lambdas_gathered
            self.status_applied = True
        if self.abort:
            self.score += 25 * self.lambdas_gathered
            self.status_applied = True
        if self.map_time_tracker[self.x_bot][self.y_bot + 1 ] == 1:
            self.loose = True
            self.status_applied = True
        if self.debug:
            print "Under Water: " + str(self.current_underwater_time)
            print "Water Proof: " + str(self.waterproof)
            print "Water level: " + str(self.current_water)
            print "Current Y: " + str(self.y_bot)
        if self.current_underwater_time == self.waterproof:
            self.loose = True
            self.status_applied = True
        

class Bot:
    def __init__(self, minemap):
        self.debug = True
        self.current_minemap = MineCraft(minemap)
        self.command = ''
        self.last_command = ''
        self.sleep_count = 0
        self.sleep_count_max = self.current_minemap.y_max - 1 # there is no sense to wait longer .. 

    def death_or_win_check(self, cmd):
        test_minemap = copy.deepcopy(self.current_minemap)
        
        x_bot = test_minemap.x_bot
        y_bot = test_minemap.y_bot
        lambdas_gathered = test_minemap.lambdas_gathered
        
        #-> attempt to make a turn
        test_minemap.turn(cmd)
        # no changes, turn seems to be invalid
        if test_minemap.x_bot == x_bot and test_minemap.y_bot == y_bot and cmd != 'W':
            return 'invalid'

        # will we die? or win?
        test_minemap.rebuild()
        test_minemap.end_of_turn()
        if test_minemap.win:
            return 'win'
        elif test_minemap.loose:
            return 'loose'
        
        # will we take lambda? if 'yes', then we go in this way
        if lambdas_gathered < test_minemap.lambdas_gathered:
            return 'lambda_plus'

        return 'ok'
        
    def get_priorities(self, x_bot, y_bot, coordinates):
        priorities = []
        if x_bot < coordinates[0]:
            priorities.append('R')
        elif x_bot > coordinates[0]:
            priorities.append('L')
        else:
            pass
        
        if y_bot < coordinates[1]:
            priorities.append('U')
        elif y_bot > coordinates[1]:
            priorities.append('D')
        else:
            pass
        return priorities

    def analyze_targets(self, x_bot, y_bot, targets):
        distance = 100
        priorities =[]
        for coordinates in targets:
            d = abs(x_bot - coordinates[0]) + abs(y_bot - coordinates[1])
            if d < distance:
                distance = d
                priorities = self.get_priorities(x_bot, y_bot, coordinates)
        return priorities

    def scan(self):
        level = 3
        
        lambdas = []
        razors = []
        trampolines = []

        x_bot = self.current_minemap.x_bot
        y_bot = self.current_minemap.y_bot
        
        for k in range(0, level):
            for l in range(0, level):
                x_new = x_bot + k
                y_new = y_bot + l
                if x_new <= self.current_minemap.x_max and y_new <= self.current_minemap.y_max:
                    if self.current_minemap.minemap[y_new][x_new] == '\\':
                        lambdas.append([x_new, y_new])
                    elif self.current_minemap.minemap[y_new][x_new] == '!':
                        razors.append([x_new, y_new])
                    elif self.current_minemap.minemap[y_new][x_new] in ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I']:
                        trampolines.append([x_new, y_new])

        priorities = []
        if lambdas.__len__() > 0:
            priorities = self.analyze_targets(x_bot, y_bot, lambdas)
        elif razors.__len__() > 0:
            priorities = self.analyze_targets(x_bot, y_bot, razors)
        elif trampolines.__len__() > 0:
            priorities = self.analyze_targets(x_bot, y_bot, trampolines)
        return priorities
                
    def get_possible_turns(self):
        #XXX: crazy
        possible_turns = []
        moves = [ 'U', 'D', 'L', 'R' ]
        priorities = self.scan()

        for cmd in moves:
            test_res = self.death_or_win_check(cmd)
            if test_res == 'invalid':
                continue
            elif test_res == 'win':
                possible_turns = [cmd]
                break
            elif test_res == 'loose':
                continue
            elif test_res == 'lambda_plus':
                possible_turns = [cmd]
                break
            elif test_res == 'ok':
                if priorities.__len__() > 0 and cmd in priorities:
                    possible_turns = [cmd]
                    break
                else:
                    possible_turns.append(cmd)
            else:
                if self.debug:
                    print '!!!!!!!!Unknown test results'

        sleep_test_res  = self.death_or_win_check('W') 

        if self.current_minemap.razors > 0 and sleep_test_res == 'ok':
            w = False
            for k in range(-1, 2):
                for l in range(-1,2):
                    x_bot = self.current_minemap.x_bot
                    y_bot = self.current_minemap.y_bot
                    if self.current_minemap.minemap[y_bot + l][x_bot+k] == 'W':
                        w = True
            if w:
                possible_turns.append('S')

        if possible_turns.__len__() == 0:
            if sleep_test_res == 'loose':
                possible_turns = ['A']
            else:
                possible_turns = ['W']
                self.sleep_count += 1

        if self.sleep_count == self.sleep_count_max:
            possible_turns = ['A']

        if self.current_minemap.size == self.current_minemap.turn_number + 2:
            possible_turns = ['A']

        if self.debug:
            print possible_turns
        return possible_turns

    def cmd_back(self,command):
        if command == 'U':
            return 'D'
        elif command == 'D':
            return 'U'
        elif command == 'L':
            return 'R'
        elif command == 'R':
            return 'L'
        else:
            return ''

    def choose_turn(self, turns):
        possible_turns = []
        excluded_cmd = ''
        for turn in turns:
            if turn != self.cmd_back(self.last_command):
                possible_turns.append(turn)
            else:
                excluded_cmd = turn

        if possible_turns.__len__() > 1:
            return possible_turns[random.randint(0, possible_turns.__len__() - 1)]
        elif possible_turns.__len__() == 1:
            return possible_turns[0]
        elif possible_turns.__len__() == 0:
            return excluded_cmd
        else:
            raise 'something bad happened'

    def do_turn(self):
        possible_turns = self.get_possible_turns()
        cmd = self.choose_turn(possible_turns)
        self.last_command = cmd
        self.command += cmd

        if cmd != 'W':
            self.sleep_count = 0

        if self.debug:
            print self.command
        self.current_minemap.turn(cmd)
        self.current_minemap.rebuild()
        self.current_minemap.end_of_turn()
        
        if self.current_minemap.win or self.current_minemap.abort or self.current_minemap.loose:
            return 'done'
        else:
            return 'running'




            




if __name__ == '__main__':
    def handler(signum, frame):
        print_result()
    def print_result():
        print best_command
        #print best_score
        sys.exit(1)
    signal.signal(signal.SIGINT, handler)

    production = True 
    if production:
        minemap = []
        s_buf = ''
        for line in sys.stdin:
            s_buf += line
        minemap = s_buf.splitlines()

        best_score = 0
        best_command = 'A'

        attempts = 200
        while True:
            bot = Bot(minemap)
            bot.debug = False
            bot.current_minemap.debug = False
            while True:
                res = bot.do_turn()
                if bot.current_minemap.score > best_score:
                    best_score = bot.current_minemap.score
                    best_command = bot.command
                if res == 'done':
                    #print bot.command
                    #print bot.current_minemap.score
                    break
            attempts -= 1
            if attempts == 0:
                break

        print_result()

        print '******'
        print "BEST:"
        print best_command
        #print best_score

    else:
        if sys.argv.__len__() < 2:
            print 'Usage:\n' +  'to re-play ' + __file__ + ' <map> <command|file with command>\n' + 'to play with bot ' + __file__ + ' <map> '
            sys.exit(1)
        map_file = sys.argv[1]

        minemap = []
        if not os.path.exists(map_file):
            print map_file + ': no such file or directory'
            sys.exit(1)
        else:
            with open(map_file, 'r') as f:
                lines = f.read()
            f.closed
            minemap = lines.splitlines()

        if sys.argv.__len__ () == 2:
            bot = Bot(minemap)
            while True:
                res = bot.do_turn()
                for j in xrange(bot.current_minemap.y_max, -1, -1):
                    print ''.join(bot.current_minemap.minemap[j])
                print 'LAMBDAS: ' + str(bot.current_minemap.lambdas_gathered) + '/' + str(bot.current_minemap.lambdas_total)
                print 'SCORE: ' + str(bot.current_minemap.score)
                print 'WIN/ABORT/LOOSE: ' + str(bot.current_minemap.win) + '/' + str(bot.current_minemap.abort) + '/' + str(bot.current_minemap.loose)
                print 'TURN: ' + str(bot.current_minemap.turn_number)
                print 'CURRENT GROWTH: ' + str(bot.current_minemap.current_growth)
                print 'RAZORS: ' + str(bot.current_minemap.razors)
                if res == 'done':
                    break
                
        else:
            command = ''
            if not os.path.exists(sys.argv[2]):
                command = sys.argv[2]
            else:
                f = open(sys.argv[2], 'r')
                command =  f.readline

            minecraft = MineCraft(minemap)

            #print minecraft.minemap[1][1]
            #print minecraft.y_max
            #print minecraft.x_max
            #print minecraft.x_bot
            #print minecraft.y_bot
            for j in xrange(minecraft.y_max, -1, -1):
                print ''.join(minecraft.minemap[j])

            for cmd in list(command):
                minecraft.turn(cmd)
                minecraft.rebuild()
                minecraft.end_of_turn()

                for j in xrange(minecraft.y_max, -1, -1):
                    print ''.join(minecraft.minemap[j])

                for j in xrange(minecraft.y_max, -1, -1):
                    s = ''
                    for i in xrange(minecraft.x_max, -1, -1):
                        s += str(minecraft.map_time_tracker[i][j])
                    print s

                print 'LAMBDAS: ' + str(minecraft.lambdas_gathered) + '/' + str(minecraft.lambdas_total)
                print 'SCORE: ' + str(minecraft.score)
                print 'WIN/ABORT/LOOSE: ' + str(minecraft.win) + '/' + str(minecraft.abort) + '/' + str(minecraft.loose)
                print 'TURN: ' + str(minecraft.turn_number)
                print 'CURRENT GROWTH: ' + str(minecraft.current_growth)
                print 'RAZORS: ' + str(minecraft.razors)
