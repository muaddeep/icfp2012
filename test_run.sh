#!/bin/bash

# diesel@ip-10-48-129-168:~/icfp$ ls maps/ | while read i; do  ./test_run.sh $i; done
echo -ne "$1 " >> results.log

if [ x"$2" != "x" ]; then
	timeout=$2
else
	timeout=120
fi

cat ./maps/$1 | timeout -s INT -k 10s ${timeout}s python temp.py >> results.log
